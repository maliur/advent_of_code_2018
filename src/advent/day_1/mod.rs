use std::collections::HashSet;
use std::fs;

pub fn solve() {
    let input_path =
        String::from("/home/maliur/projects/advent_of_code_2018/src/advent/day_1/input.txt");
    let input = fs::read_to_string(&input_path).expect("could not read from file");

    println!("Answer for part one is {:?}", part_one(&input));
    println!("Answer for part two is {:?}", part_two(&input));
}

fn part_one(input: &str) -> i32 {
    return input
        .lines()
        .filter_map(|l| l.parse::<i32>().ok())
        .fold(0, |acc, n| acc + n);
}

fn part_two(input: &str) -> i32 {
    let mut seen: HashSet<i32> = HashSet::new();
    let mut frequency = 0;
    seen.insert(frequency);
    let numbers = input.lines().filter_map(|l| l.parse::<i32>().ok()).cycle();

    for n in numbers {
        frequency += n;
        if seen.contains(&(frequency)) {
            return frequency;
        } else {
            seen.insert(frequency);
        }
    }
    return 0;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let plus = "+1\n+1\n+1";
        let minus = "-1\n-2\n-3";
        let plus_minus = "-1\n+2\n-1";

        assert_eq!(part_one(plus), 3);
        assert_eq!(part_one(minus), -6);
        assert_eq!(part_one(plus_minus), 0);
    }

    #[test]
    fn test_part_two() {
        let input = "+1\n-1";
        let input_2 = "+3\n+3\n+4\n-2\n-4";
        let input_3 = "-6\n+3\n+8\n+5\n-6";
        let input_4 = "+7\n+7\n-2\n-7\n-4";

        assert_eq!(part_two(input), 0);
        assert_eq!(part_two(input_2), 10);
        assert_eq!(part_two(input_3), 5);
        assert_eq!(part_two(input_4), 14);
    }
}
