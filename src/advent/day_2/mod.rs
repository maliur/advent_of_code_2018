use std::fs;

pub fn solve() {
    let input_path =
        String::from("/home/maliur/projects/advent_of_code_2018/src/advent/day_2/input.txt");
    let input = fs::read_to_string(&input_path).expect("could not read from file");

    println!("Answer for part one is {:?}", part_one(&input));
}

fn part_one(input: &str) -> usize {
    let twice = input
        .lines()
        .filter(|word| has_number_of_duplicates(word, 2))
        .count();

    let thrice = input
        .lines()
        .filter(|word| has_number_of_duplicates(word, 3))
        .count();

    twice * thrice
}

fn has_number_of_duplicates(word: &str, duplicates: usize) -> bool {
    unimplemented!()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = "abcdef\nabbcde\nabcccd\nbababc";

        assert_eq!(part_one(input), 12);
    }
}
